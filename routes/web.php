<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('products','ProductController@index')->name('product.index');
//Route::get('products/create','ProductController@create')->name('product.create');
//Route::get('products/edit/{id}', 'ProductController@edit')->name('product.edit');
//Route::post('products/edit/{id}','ProductController@update');
//Route::post('products/create','ProductController@store');
//Route::delete('products/destroy/{id}', 'ProductController@destroy')->name('product.destroy');
//Route::get('products/show/{id}', 'ProductController@show')->name('product.show');


Route::resource('products', 'ProductController');

