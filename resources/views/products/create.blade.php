@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">افزودن محصول</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">نام محصول</label>
                                <input name="name" id="name" class="form-control" type="text">
                            </div>
                            <div class="form-group">
                                <label for="stock">موجودی انبار</label>
                                <input name="stock" id="stock" class="form-control" type="text">
                            </div>
                            <div class="form-group">
                                <label for="price">قیمت</label>
                                <input name="price" id="price" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="barcode">بارکد</label>
                                <input name="barcode" id="barcode" class="form-control" type="text">
                            </div>
                            <div class="form-group">
                                <label for="product_category_id">دسته بندی</label>
                                <input name="product_category_id" id="product_category_id" class="form-control" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="{{ route('products.index') }}" type="submit" class="btn btn-primary">لفو</a>
                    <button type="submit" class="btn btn-primary">ثبت محصول</button>
                </div>
            </div>
        </form>
    </div>
@endsection