@extends('layouts.app')
@section('content')
    <h2>لیست محصولات</h2>
    <a href="{{ route('products.create') }}"><h5>افزودن محصول</h5></a>
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>نام محصول</th>
            <th>موجودی انبار</th>
            <th>قیمت</th>
            <th>بارکد</th>
            <th>دسته بندی</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->stock }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->barcode }}</td>
                <td>Halo badan</td>
                <td>
                    <a href="{{ route('products.edit', $product->id) }}" class="btn btn-warning btn-xs" title="ویرایش">
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    <form style="display: inline" action="{{ route('products.destroy', $product->id) }}" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="_method" value="DELETE">
                        <button onclick="return confirm('are u sure?')" type="submit" class="btn btn-danger btn-xs" title="حذف">
                            <i class="glyphicon glyphicon-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection