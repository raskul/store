<?php


namespace App\Classes;


trait DateConvert
{
    public function toGregorian($jalali_date){
        $jalali_date = $this->toEnglishNumbers($jalali_date);
        $jalali_arr = explode('/', $jalali_date);
        if (count($jalali_arr) < 3) {
            return null;
        }
        if (\Morilog\Jalali\jDateTime::checkDate($jalali_arr[0], $jalali_arr[1], $jalali_arr[2], true) == false) {
            return null;
        }
        $gregorian_arr = \Morilog\Jalali\jDateTime::toGregorian($jalali_arr[0], $jalali_arr[1], $jalali_arr[2]);
        return implode('-', $gregorian_arr);
    }

    public function toJalali($gregorian_date){
        $gregorian_arr = explode('-', $gregorian_date);
        if (count($gregorian_arr) < 3) {
            return null;
        }
        if (\Morilog\Jalali\jDateTime::checkDate($gregorian_arr[0], $gregorian_arr[1], $gregorian_arr[2], false) == false) {
            return null;
        }
        $jalali_arr = \Morilog\Jalali\jDateTime::toJalali($gregorian_arr[0], $gregorian_arr[1], $gregorian_arr[2]);
        return implode('/', $jalali_arr);
    }

    /**
     * convert persian and arabic numbers to english number
     *
     * @param $string
     * @return mixed
     */
    function toEnglishNumbers($string) {
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

        $num = range(0, 9);
        $convertedPersianNums = str_replace($persian, $num, $string);
        $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

        return $englishNumbersOnly;
    }

    /**
     * convert english numbers to persian numbers
     *
     * @param $string
     * @return mixed
     */
    public function toPersianNumbers($string)
    {
        $farsi_array = array("۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹");
        $english_array = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

        return str_replace($english_array, $farsi_array, $string);
    }
}