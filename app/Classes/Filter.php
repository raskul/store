<?php


namespace App\Classes;


use App\Classes\Helpers\DateAndNumber;

trait Filter
{
    public function scopeFilter($query, $data)
    {
        foreach ($this->filterable as $key => $value) {
            switch ($value) {
                case 'from_to':
                    if (key_exists($key . '_from', $data) && !empty($data[$key . '_from'])) {
                        $field = DateAndNumber::toEnglishNumbers($data[ $key . '_from' ]);
                        $field = DateAndNumber::toEnglishNumbers($data[ $key . '_from' ]);
                        $query->where($key, '>=', $field);
                    }
                    if (key_exists($key . '_to', $data) && !empty($data[$key . '_to'])) {
                        $field = DateAndNumber::toEnglishNumbers($data[ $key . '_to' ]);
                        $query->where($key, '<=', $field);
                    }
                    break;
                case 'jalali_from_to':
                    if (key_exists($key . '_from', $data) && !empty($data[$key . '_from'])) {
                        $field = DateAndNumber::toGregorian($data[ $key . '_from' ]);
                        $query->where($key, '>=', $field);
                    }
                    if (key_exists($key . '_to', $data) && !empty($data[$key . '_to'])) {
                        $field = DateAndNumber::toGregorian($data[ $key . '_to' ]);
                        $query->where($key, '<=', $field);
                    }
                    break;
                case 'in':
                    if (key_exists($key, $data) && !empty($data[$key])) {
                        $query->whereIn("{$key}", $data[ $key ]);
                    }
                    break;
                case 'equal':
                    if (key_exists($key, $data) && !empty($data[$key])) {
                        $query->where("{$key}", $data[ $key ]);
                    }
                    break;
                case 'like':
                    if (key_exists($key, $data) && !empty($data[$key])) {
                        $query->where("{$key}", 'like', '%' . $data[ $key ] . '%');
                    }
                    break;
                case 'left_like':
                    if (key_exists($key, $data) && !empty($data[$key])) {
                        $query->where("{$key}", 'like', '%' . $data[ $key ]);
                    }
                    break;
                case 'right_like':
                    if (key_exists($key, $data) && !empty($data[$key])) {
                        $query->where("{$key}", 'like', $data[ $key ] . '%');
                    }
                    break;
            }
        }

        return $query;
    }
}