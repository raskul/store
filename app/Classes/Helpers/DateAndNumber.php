<?php


namespace App\Classes\Helpers;


use Morilog\Jalali\jDateTime;

class DateAndNumber
{
    public static function toJalali($gregorian_date){
        $gregorian_arr = explode('-', $gregorian_date);
        if (count($gregorian_arr) < 3) {
            return null;
        }
        if (jDateTime::checkDate($gregorian_arr[0], $gregorian_arr[1], $gregorian_arr[2], false) == false) {
            return null;
        }
        $jalali_arr = jDateTime::toJalali($gregorian_arr[0], $gregorian_arr[1], $gregorian_arr[2]);
        return implode('/', $jalali_arr);
    }

    public static function toGregorian($jalali_date){
        $jalali_date = self::toEnglishNumbers($jalali_date);
        $jalali_arr = explode('/', $jalali_date);
        if (count($jalali_arr) < 3) {
            return null;
        }
        if (jDateTime::checkDate($jalali_arr[0], $jalali_arr[1], $jalali_arr[2], true) == false) {
            return null;
        }
        $gregorian_arr = jDateTime::toGregorian($jalali_arr[0], $jalali_arr[1], $jalali_arr[2]);
        return implode('-', $gregorian_arr);
    }

    /**
     * convert persian and arabic numbers to english number
     *
     * @param $string
     * @return mixed
     */
    public static function toEnglishNumbers($string) {
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

        $num = range(0, 9);
        $convertedPersianNums = str_replace($persian, $num, $string);
        $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

        return $englishNumbersOnly;
    }

    /**
     * convert english numbers to persian numbers
     *
     * @param $string
     * @return mixed
     */
    public static function toPersianNumbers($string)
    {
        $farsi_array = array("۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹");
        $english_array = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

        return str_replace($english_array, $farsi_array, $string);
    }

    public static function arabicToPersian($string)
    {
        $characters = [
            'ك' => 'ک',
            'دِ' => 'د',
            'بِ' => 'ب',
            'زِ' => 'ز',
            'ذِ' => 'ذ',
            'شِ' => 'ش',
            'سِ' => 'س',
            'ى' => 'ی',
            'ي' => 'ی',
            '١' => '۱',
            '٢' => '۲',
            '٣' => '۳',
            '٤' => '۴',
            '٥' => '۵',
            '٦' => '۶',
            '٧' => '۷',
            '٨' => '۸',
            '٩' => '۹',
            '٠' => '۰',
        ];
        return str_replace(array_keys($characters), array_values($characters),$string);
    }
}